# optimize for debug
CFLAGS += -O0
# to better detect programming errors
CFLAGS += -fstack-protector-all
# add debug symbols (remove for smaller release)
CFLAGS += -ggdb
# use GNU99 (POSIX not included in C99 is required for sockets)
CFLAGS += -std=gnu99
# have strict warning (for better code)
CFLAGS += -Wpedantic -Wall -Werror -Wundef -Wextra -Wshadow -Wredundant-decls -Wmissing-prototypes -Wstrict-prototypes -Wstrict-overflow=5
# add options for better code optimization
CFLAGS += -fno-common -ffunction-sections -fdata-sections

all: u2_usb u2_bt

u2_usb: u2_usb.c
	gcc ${CFLAGS} `pkg-config --cflags hidapi-libusb` -o $@ $< `pkg-config --libs hidapi-libusb`

u2_bt: u2_bt.c
	gcc ${CFLAGS} `pkg-config --cflags dbus-1` -o $@ $< `pkg-config --libs dbus-1`
