these Linux programs read out the measurements from the WEB/WITRN/GZUt/QWay U2/U2p USB power meter.

go to [wiki](https://wiki.cuvoodoo.info/doku.php?id=web-u2) for more information about the device and protocols.

USB
===

`u2_usb` reads the measurements using the USB HID interface.

to compile the source code you require: gcc, make, hidapi library (libhidapi-dev).
then just run `make` and it will output the `u2_usb` binary.

run `u2_usb` to get the output in CSV format.
run `u2_usb -d` to get the raw and decoded output (useful for reversing and debugging).

if you get the message "could not open WEB-U2 (make sure it is connected and you have access rights)", then provide access rights to this USB device the following way:
~~~
sudo cp 99-web-u2.rules /etc/udev/rules.d/
sudo udevadm control --reload-rules
sudo udevadm trigger
~~~
and re-plug the device.

additionally `u2_bt` can send the voltage/current measurements to an InfluxDB time series database over UDP.
provide the InfluxDB hostname/IP and UDP port as arguments.

Bluetooth
=========

to compile the source code you require: gcc, make, dbus library (libdbus-1-dev).
then just run `make` and it will output the `u2_bt` binary.

ensure the BlueZ Bluetooth stack is running:
```
sudo systemctl start bluetooth
```
don't forget to grant the permissions to access bluetooth devices:
```
sudo adduser $USER bluetooth
```

run `u2_bt` to get the output in CSV format.
run `u2_bt -d` to get the raw and decoded output (useful for reversing and debugging).

I did not implement the possibility to specify the Bluetooth adapter and device.
the program uses the first Bluetooth adapter it finds, and first device with the name starting with Qway_U2.

additionally `u2_bt` can send the energy measurements to an InfluxDB time series database over UDP.
provide the InfluxDB hostname/IP and UDP port as arguments.
